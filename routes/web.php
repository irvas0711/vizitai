<?php

use App\Http\Controllers\LandingController;
use App\Http\Controllers\AuthOrganization;
use App\Http\Controllers;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VisitApplicationController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/login', function (){
    //
})->middleware('homeauth');
*/

// AUTH Routes
Auth::routes();

// USER Routes
Route::resource('users', UserController::class);
Route::get('/home', [UserController::class, 'index'])->name('home');
Route::get('goodbye-user',
    [UserController::class, 'deleteUser'])->name('user.delete');

// Static routes
Route::get('/', [LandingController::class, '__invoke'])->name('landing');

// APPLICATION ROUTES
Route::get('/show-organizations',
    [VisitApplicationController::class, 'showAll']);
Route::get('/application/{organization_id}',
    [VisitApplicationController::class, 'show'])->name('show');
Route::post('/apply/{organization_id}/{model}/{model_id}',
    [\App\Http\Controllers\VisitApplicationController::class, 'contact'])->name('organization.contact');

// BI CALCULATIONS
Route::get('/statistics',
    [OrganizationController::class, 'calculateStatistics'])->name('organizations.statistics');
Route::get('/for-visiters',
    [UserController::class, 'informationPage'])->name('users.intro');
Route::get('/for-organizations',
    [OrganizationController::class, 'informationPage'])->name('organizations.intro');


Route::get('/list', function () {
    return view('applications.search');
});

// Organization Authentication Routes
Route::get('organizations/login', [AuthOrganization\LoginController::class, 'showLoginForm'])->name('organization.login');
// ->middleware('homeauth');
Route::post('organizations/login', [AuthOrganization\LoginController::class, 'login']);
// ->middleware('homeauth');
Route::post('organizations/logout', [AuthOrganization\LoginController::class, 'logout'])->name('organization.logout');

// Organization Registration Routes
Route::get('organizations/register', [AuthOrganization\RegisterController::class, 'showRegistrationForm'])->name('organization.register');
// ->middleware('homeauth');
Route::post('organizations/register', [AuthOrganization\RegisterController::class, 'register']);
// ->middleware('homeauth');

// ORGANIZATION PASSWORD RESET ROUTES
Route::get('organizations/password/reset',
    [AuthOrganization\ForgotPasswordController::class, 'showLinkRequestForm'])
    ->name('organization.password.request');

Route::get('organizations/password/reset/{token}',
    [AuthOrganization\ResetPasswordController::class, 'showResetForm'])
    ->name('organization.password.reset');

Route::post('organizations/password/email',
    [AuthOrganization\ForgotPasswordController::class, 'sendResetLinkEmail'])
    ->name('organization.password.email');

Route::post('organizations/password/reset',
    [AuthOrganization\ResetPasswordController::class, 'reset'])
->name('organizations.password.update');

// Organization Profile Route
Route::get('organization/applications',
    [OrganizationController::class, 'showApplications'])->name('organization.applications');
Route::get('organization/profile',
    [OrganizationController::class, 'showFullProfile'])->name('organization.profile');
Route::get('organization/edit',
    [OrganizationController::class, 'editFullProfile'])->name('organization.edit');
Route::put('organization/edit',
    [OrganizationController::class, 'updateFullProfile'])->name('organization.update');
Route::get('goodbye',
    [OrganizationController::class, 'deleteOrganization'])->name('organization.delete');

// Admin User Panel Routes
Route::get('/panel', function () {
    return view('admin.panel');
});

# ADMIN PANEL TO MANAGE ORGANIZATIONS
Route::resource('organizations',
    OrganizationController::class);
/*
Route::get('/admin', [UserController::class, 'adminPanel']);

*/
# STATIC ROUTES
/*
Route::get('/{documents}','StaticPageController')
    ->name('document')
    ->where('documents','intro');

Route::get('/confirm', function () {
    return view('admin.confirm');
});
*/
