<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MailResetPasswordToken extends Notification
{
    use Queueable;

    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("Švietimo įstaigos registracija")
                    ->line("Sveikiname, jūs gavote registracijos kvietimą į Švietimo įstaigų vizitavimo sistemą.
            Norėdami užbaigti registraciją, sekite žemiau nurodyta nuoroda. Daugiau informacijos
            apie mūsų projektą rasite adresu vizitai.local")
                    ->action('Sukurti slaptažodį', $this->url)
                    ->line('Ačiū, kad esate vizitavimo sistemos dalis.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
