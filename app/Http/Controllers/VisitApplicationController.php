<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use App\Models\User;
use App\Models\VisitApplication;
use Gate;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Http\Response;
use Mail;

class VisitApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     * @return Application|Factory|View|Response
     */
    public function showAll(Request $request)
    {
        $kind = null;
        $city = null;
        $area = null;
        $preschool = null;
        $primary = null;
        $midst = null;
        $gymnasium = null;

        $request->validate([
            'city' => 'string|min:3|max:22|nullable',
            'area' => 'string|min:3|max:88|nullable',
            'kind' => 'string|min:6|max:22|nullable',
        ]);

        if ($request->get('search') == 1) {

            $queryBuilder = Organization::query();
            // tikrina, ar ieškoma pagal savivaldybę
            if ($request->has('area') && $request->get('area') != null) {
                $area = $request->get('area'); // pasiimama paieškos reikšmę
                $queryBuilder->searchArea($area); // filtruoja paieškos rezultatus
            }

            if ($request->has('city') && $request->get('city') != null) {
                $city = $request->get('city');
                $queryBuilder->searchCity($city); // $queryBuilder = $queryBuilder->where('city', $city);
            }
            if ($request->has('kind') && $request->get('kind') != null) {
                $kind = $request->get('kind');
                $queryBuilder->searchKind($kind); // $queryBuilder = $queryBuilder->where('kind', $kind);
            }

            if ($request->has('preschool') && $request->get('preschool') == 1) {
                $queryBuilder->searchIsPreschool();
                $preschool = 1;
            }
            if ($request->has('primary') && $request->get('primary') == 1) {
                $queryBuilder->searchIsPrimary();
                $primary = 1;
            }
            if ($request->has('midst') && $request->get('midst') == 1) {
                $queryBuilder->searchIsMidst();
                $midst = 1;
            }
            if ($request->has('gymnasium') && $request->get('gymnasium') == 1) {
                $queryBuilder->searchIsGymnasium();
                $gymnasium = 1;
            }
            $organizations = $queryBuilder->get()->toArray();
            $count = count($organizations);
        } else {
            $organizations = Organization::all()->toArray();
            $count = count($organizations);
        }


        $dropdownOrgs = Organization::all()->toArray();
        $areas = [];
        $kinds = [];
        $cities = [];

        $i = 0;
        $j = 0;
        $z = 0;
        // Form dropdowns
        foreach ($dropdownOrgs as $organization) {
            if (in_array($organization['area'], $areas) == false) {
                $areas[$i] = $organization['area'];
                $i++;
            }
            if (in_array($organization['kind'], $kinds) == false) {
                $kinds[$z] = $organization['kind'];
                $z++;
            }
            if (in_array($organization['city'], $cities) == false) {
                $cities[$j] = $organization['city'];
                $j++;
            }
        }
        // $organizations = Organization::latest()->paginate(15);
        // $organizations = Organization::latest()->simplePaginate(10);
        return view('applications.search',
            compact('organizations', 'areas', 'kinds', 'cities', 'kind', 'city', 'area',
                'preschool', 'primary', 'midst', 'gymnasium', 'count'));
        // ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Display the specified resource.
     *
     * @param $organization
     * @return Application|Factory|View|Response
     */
    public function show($organization)
    {
        $organization = Organization::findOrFail($organization);
        if (Auth::check()) {
            $model = 'user';
            $model_id = Auth::user()->id;
        } elseif (Auth::guard('organization')->check()) {
            $model = 'organization';
            $model_id = Auth::guard('organization')->user()->id;
        } else {
            $model = 'guest';
            $model_id = null;
        }

        //   var_dump($model);
        //   var_dump($model_id);
        return view('applications.show',
            compact('organization', 'model', 'model_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param $id
     * @param $model
     * @param $model_id
     * @param $visitor
     * @return Application|Factory|View|void
     */
    public function contact(Request $request, $id, $model, $model_id)
    {
        // if (Gate::allows('logged-in-user') || Gate::allows('logged-in-organization')) {
        if (Auth::guard('organization')->check() || Auth::guard('web')->check()) {
            $request->validate([
                "name" => "required | min:12 | max:1200",
                "description" => "required | min:32 | max:3200"
            ]);

            // sukuriamas naujas įrašas kaip objektas duomenų lentelėje
            $visitApplication = new VisitApplication();
            // įvesties reikšmė priskiriama about stulpeliui
            $visitApplication->about = $request->get("name");

            /** @var TYPE_NAME $visitApplication */
            $visitApplication->applicable = $model;
            $visitApplication->applicable_id = $model_id;
            $visitApplication->organization_id = $id;
            $visitApplication->description = $request->get("description");

            if ($visitApplication->applicable == 'organization')
                $visitor = Organization::findOrFail($visitApplication->applicable_id);
            else $visitor = User::findOrFail($visitApplication->applicable_id);

            if ($visitApplication->save()) {
                Mail::send('emails.invite_plain', [
                    'visitor' => $visitor,
                    'visitApplication' => $visitApplication],
                    function ($mail) use ($visitApplication) {
                        $organization = Organization::findOrFail($visitApplication->organization_id);
                        $email = $organization->email;
                        if ($email) {
                            $mail->to($email)->subject('Nauja vizitavimo užklausa');
                        }
                    });
            }

            return redirect('/application/' . $id)->with("success", "Jūs sėkmingai aplikavote vizitui!
        Įstaiga su jumis susisieks. Pateiktas vizitavimo užklausas galite peržiūrėti savo profilyje.");

        } else
            return abort(403, 'Neturite prieigos');
    }


}
