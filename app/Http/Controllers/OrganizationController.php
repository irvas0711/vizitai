<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Organization;
use App\Models\User;
use App\Models\VisitApplication;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Notifications\MailResetPasswordToken;
use Illuminate\Notifications\Notifiable;
use phpDocumentor\Reflection\Utils;

/*
use Gate;
use Hash;
use Illuminate\Routing\Redirector;
*/


class OrganizationController extends Controller
{
    use Notifiable;

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        if (\Gate::allows('admin-actions')) {
            $organizations = Organization::latest()->simplePaginate(5);

            return view('organizations.manage.index', compact('organizations'))
                ->with('i', (request()->input('page', 1) - 1) * 5);
        } else return redirect('/')->with('error', 'Neturite teisių');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        if (\Gate::allows('admin-actions')) {
            return view('organizations.manage.create');
        } else return redirect('/')->with('error', 'Neturite teisių');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|void
     */
    public function storeFullProfile(Request $request)
    {
        if (\Gate::allows('admin-actions')) {
            $request->validate([
                'name' => 'string|required|min:12|max:260',
                'email' => 'string|required|email',
                'city' => 'string|required|min:3|max:22',
                'area' => 'string|required|min:3|max:88',
                'kind' => 'string|required|min:6|max:22',
                'classes' => 'string|required|min:1|max:16',
                'about' => 'string|required|min:20|max:1200',
                'good_examples' => 'string|required|min:20|max:3600',
            ]);

            Organization::create($request->all());

            return redirect()->route('organization.profile')
                ->with('success', 'Švietimo įstaiga sėkmingai pridėta');
        } else return redirect('/')->with('error', 'Neturite teisių');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse|void
     */
    public function store(Request $request)
    {
        if (\Gate::allows('admin-actions')) {
            $request->validate([
                'name' => 'required',
                'email' => 'string|required|email|unique:users',
            ]);

            Organization::create($request->all());

            return redirect()->route('organizations.index')
                ->with('success', 'Švietimo įstaiga sėkmingai pridėta');
            # return redirect('/users')->with('success', 'You have created a user.');
        } else return redirect('/')->with('error', 'Neturite teisių');
    }

    /**
     * Display the specified resource.
     *
     * @param Organization $organization
     * @return Application|Factory|View|Response
     */
    public function showFullProfile(Organization $organization)
    {
        # dd(Auth::guard('organization')->user()->id);
        if (Auth::guard('organization')->check()) {
            $organization = Auth::guard('organization')->user();
            return view('organizations.manage_profile.profile', compact('organization'));
        } else return redirect('/')->with('error', 'Neturite teisių');
    }

    /**
     * Display the specified resource.
     *
     * @param Organization $organization
     * @return Application|Factory|View|Response
     */
    public function show(Organization $organization)
    {
        if (\Gate::allows('admin-actions')) {
            return view('organizations.manage.show', compact('organization'));
        } else return redirect('/')->with('error', 'Neturite teisių');
    }

    /**
     * Show the form for editing the specified resource.
     * @param Request $request
     * @param Organization $organization
     * @return Application|Factory|View|Response|void
     */
    public function editFullProfile(Request $request, Organization $organization)
    {
        if (Auth::guard('organization')->check()) {
            $areas = Area::all();
            $allKinds = ['Valstybinė', 'Privati', 'Alternatyvaus švietimo', 'Jaunimo centras'];
            $organization = Auth::guard('organization')->user();
            $current_area = $organization->area;
            $current_kind = $organization->kind;

            return view('organizations.manage_profile.edit',
                compact('organization', 'areas', 'current_area', 'current_kind', 'allKinds'));
        } else
            return abort(403, 'Neturite prieigos');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Organization $organization
     * @return Application|RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function edit(Organization $organization)
    {
        if (\Gate::allows('admin-actions')) {
            return view('organizations.manage.edit', compact('organization'));
        } else return redirect('/')->with('error', 'Neturite teisių');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Organization $organization
     * @return RedirectResponse|void
     */
    public function updateFullProfile(Request $request, Organization $organization)
    {
        if (Auth::guard('organization')->check()) {
            $organization_id = Auth::guard('organization')->user()->id;
            $validatedData = $request->validate([
                'name' => 'string|required|min:12|max:260',
                'email' => 'string|required|email',
                'city' => 'string|required|min:3|max:22',
                'area' => 'string|required|min:3|max:88',
                'kind' => 'string|required|min:6|max:22',
                'about' => 'string|required|min:20|max:1200',
                'good_examples' => 'string|required|min:20|max:3600',
            ]);

            $update = array(
                'name' => $validatedData['name'],
                'email' => $validatedData['email'],
                'city' => $validatedData['city'],
                'area' => $validatedData['area'],
                'kind' => $validatedData['kind'],
                'about' => $validatedData['about'],
                'good_examples' => $validatedData['good_examples'],
            );

            Organization::where([
                'id' => $organization_id
            ])->update($update);

            Organization::where('id',
                $organization_id)->update(array('preschool' => $request->has('preschool')));
            Organization::where('id',
                $organization_id)->update(array('primary' => $request->has('primary')));
            Organization::where('id',
                $organization_id)->update(array('midst' => $request->has('midst')));
            Organization::where('id',
                $organization_id)->update(array('gymnasium' => $request->has('gymnasium')));

            return redirect()->route('organization.profile')
                ->with('success', 'Švietimo įstaigos profilio redagavimas sėkmingas');
        } else
            return abort(403, 'Neturite prieigos');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Organization $organization
     * @return RedirectResponse
     */
    public function update(Request $request, Organization $organization)
    {
        if (\Gate::allows('admin-actions')) {
            $request->validate([
                'name' => 'required',
                'email' => 'string|required|email|unique:users',
            ]);
            $organization->update($request->all());

            return redirect()->route('organizations.index')
                ->with('success', 'Švietimo įstaigos profilio redagavimas sėkmingas');
        } else return redirect('/')->with('error', 'Neturite teisių');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Organization $organization
     * @return RedirectResponse
     */
    public
    function destroy(Organization $organization)
    {
        if (\Gate::allows('admin-actions')) {
            $organization->delete();

            return redirect()->route('organizations.index')
                ->with('success', 'Švietimo įstaiga ištrinta sėkmingai');
        } else return redirect('/')->with('error', 'Neturite teisių');
    }

    /**
     * @return Application|Factory|View|void
     * @throws \Exception
     */
    public
    function deleteOrganization()
    {
        if (Auth::guard('organization')->check()) {
            $organization = Organization::findOrFail(Auth::guard('organization')->user()->id);
            Auth::guard('organization')->logout();
            $organization->delete();
            return view('organizations.goodbye');
        } else
            return abort(403, 'Neturite prieigos');

    }

    public
    function showApplications()
    {
        if (Auth::guard('organization')->check() || Auth::guard('web')->check()) {
            $organization_id = Auth::guard('organization')->user()->id;
            $applicationsList = VisitApplication::all();
            $organizationsList = Organization::all();
            $usersList = User::all();

            // Išsiųstos vizitavimo užklausos
            $applied = $this->calculateSentApplications($applicationsList, $organization_id, $organizationsList);
            $sentApplications = $applied[0];
            $appliedOrganizations = $applied[1];

            // Gautos užklausos iš pavienių vartotojų
            $receivedFromUsers = $this->calculateReceivedApplicationsFromUsers($applicationsList,
                $organization_id, $usersList);
            $receivedApplicationsFromUsers = $receivedFromUsers[0];
            $waitingUsers = $receivedFromUsers[1];

            // Gautos užklausos iš kitų registruotų švietimo įstaigų
            $receivedFromOrganizations = $this->calculateReceivedApplicationsFromOrganizations($applicationsList,
                $organization_id, $organizationsList);
            $receivedApplicationsFromOrganizations = $receivedFromOrganizations[0];
            $waitingOrganizations = $receivedFromOrganizations[1];

            return view('organizations.manage_profile.showApplications',
                compact('sentApplications', 'appliedOrganizations',
                    'receivedApplicationsFromOrganizations', 'waitingOrganizations',
                    'receivedApplicationsFromUsers', 'waitingUsers'));
        } else
            return abort(403, 'Neturite prieigos');
    }

    /**
     * @param $applicationsList
     * @param $organization_id
     * @param $organizationsList
     * @return array
     */
    private
    function calculateSentApplications($applicationsList, $organization_id, $organizationsList)
    {
        $receivedOrganizationsIdsList = [];
        $sentApplications = [];
        foreach ($applicationsList as $application) {
            if ($application->applicable == 'organization' && $application->applicable_id == $organization_id) {
                array_push($sentApplications, $application);
                array_push($receivedOrganizationsIdsList, $application->organization_id);
            }
        }

        $appliedOrganizations = [];
        foreach ($receivedOrganizationsIdsList as $orgId) {
            foreach ($organizationsList as $organization) {
                if ($organization->id == $orgId) {
                    array_push($appliedOrganizations, $organization);
                }
            }
        }

        return [$sentApplications, $appliedOrganizations];
    }

    /**
     * @param $applicationsList
     * @param $organization_id
     * @param $organizationsList
     * @return array
     */
    private
    function calculateReceivedApplicationsFromOrganizations($applicationsList, $organization_id, $organizationsList)
    {
        $receivedApplicationsFromOrganizations = [];
        $receivedOrganizationsIdList = [];

        foreach ($applicationsList as $application) {
            if ($application->applicable == 'organization' && $application->organization_id == $organization_id) {
                array_push($receivedApplicationsFromOrganizations, $application);
                array_push($receivedOrganizationsIdList, $application->applicable_id);
            }
        }

        $waitingOrganizations = [];

        foreach ($receivedOrganizationsIdList as $orgId) {
            foreach ($organizationsList as $organization) {
                if ($organization->id == $orgId) {
                    array_push($waitingOrganizations, $organization);
                }
            }
        }
        //  var_dump($waitingOrganizations);
        return [$receivedApplicationsFromOrganizations, $waitingOrganizations];
    }

    /**
     * @param $applicationsList
     * @param $organization_id
     * @param $organizationsList
     * @return array
     */
    private
    function calculateReceivedApplicationsFromUsers($applicationsList, $organization_id, $userList)
    {
        $receivedApplicationsFromUsers = [];
        $receivedUsersIdList = [];

        foreach ($applicationsList as $application) {
            //    print($application);
            if ($application->applicable == 'user' && $application->organization_id == $organization_id) {
                array_push($receivedApplicationsFromUsers, $application);
                array_push($receivedUsersIdList, $application->applicable_id);
            }
        }

        $waitingUsers = [];

        foreach ($receivedUsersIdList as $userId) {
            foreach ($userList as $user) {
                if ($user->id == $userId) {
                    array_push($waitingUsers, $user);
                }
            }
        }
        // var_dump($receivedApplicationsFromUsers);
        return [$receivedApplicationsFromUsers, $waitingUsers];
    }

    /**
     * Send a password reset email to the user
     */
    public
    function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }

    public
    function countVisitApplications()
    {
        $organizations = Organization::all()->toArray();
        $visit_applications = VisitApplication::all()->toArray();
        $count_applications = 0;

        foreach ($organizations as $org) {
            foreach ($visit_applications as $application) {
                if ($org["id"] == $application["organization_id"]) $count_applications++;
                Organization::where('id',
                    $org["id"])->update(array('application_sum' => $count_applications));
            }
            $count_applications = 0;
        }
    }

    /**
     * @return Application|Factory|View
     */
    public
    function calculateStatistics()
    {
        $list = Organization::all();
        $applicationsList = VisitApplication::all();
        $organizations = $this->calculateTopOrganizations($list);
        $areaList = $this->calculateTopAreas($list);
        $kindList = $this->calculateTopKinds($list);
        $classesList = $this->calculateClasses($list, $applicationsList);

        return view('navigation.statistics',
            compact('organizations', 'areaList', 'kindList', 'classesList'));
    }

    /**
     * @param $list
     */
    private
    function calculateTopAreas($list)
    {
        $areaList = [];
        foreach ($list as $item) {
            if (array_key_exists($item->area, $areaList))
                $areaList[$item->area] = $areaList[$item->area] + $item->applications->count();
            else $areaList[$item->area] = $item->applications->count();
        }
        arsort($areaList);
        return array_slice($areaList, 0, 6);
    }

    /**
     * @param $list
     */
    private
    function calculateTopKinds($list) // $list - Organizacijų sąrašas
    {
        $kindList = [];
        foreach ($list as $item) {
            if (array_key_exists($item->kind, $kindList))  // jei masyve šis tipas yra
                // sumuoti užklausų skaičių
                $kindList[$item->kind] = $kindList[$item->kind] + $item->applications->count();
            else // jei masyve tipo nėra, žodyno principu pridėti tipą bei esamos organizacijos
                // užklausų skaičių
                $kindList[$item->kind] = $item->applications->count();
        }
        arsort($kindList); // rūšiuojama mažėjimo tvarka
        return array_slice($kindList, 0, 4); // grąžinama 10 pirmųjų
    }

    /**
     * @param $list
     */
    private
    function calculateTopOrganizations($list)
    {
        $organizations = [];
        foreach ($list as $item) {
            $organizations[$item->name] = $item->applications->count();
        }
        arsort($organizations);
        return array_slice($organizations, 0, 8);
    }

    /**
     * @param $list
     */
    private
    function calculateClasses($list, $applicationsList)
    {
        $countPreschool = 0;
        $countPrimary = 0;
        $countMidst = 0;
        $countGymnasium = 0;
        $dictionary = [];

        foreach ($applicationsList as $appItem) {
            foreach ($list as $item) {
                if ($appItem->organization_id == $item->id) {
                    if ($item->preschool == 1) $countPreschool++;
                    if ($item->primary == 1) $countPrimary++;
                    if ($item->midst == 1) $countMidst++;
                    if ($item->gymnasium == 1) $countGymnasium++;
                }
            }
        }

        $dictionary['Priešmokyklinės'] = $countPreschool;
        $dictionary['Pradinės'] = $countPrimary;
        $dictionary['5-8 klasės'] = $countMidst;
        $dictionary['Gimnazinės'] = $countGymnasium;

        arsort($dictionary);
        return array_slice($dictionary, 0, 4);
    }

    public
    function informationPage()
    {
        return view('navigation.intro');
    }
}
