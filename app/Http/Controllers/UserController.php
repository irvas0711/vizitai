<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use App\Models\User;
use App\Models\VisitApplication;
use Gate;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * @param User $user
     * @return Application|Factory|View|void
     */
    public function index(User $user)
    {
        if (Auth::guard('web')->check()) {
            $user = Auth::user();
            $user_id = $user->id;

            $applicationsList = VisitApplication::all();
            $organizationsList = Organization::all();

            $organizationsId = [];
            $sentApplications = [];

            foreach ($applicationsList as $application) {
                if ($application->applicable == 'user' && $application->applicable_id == $user_id) {
                    array_push($sentApplications, $application);
                    array_push($organizationsId, $application->organization_id);
                }
            }

            $appliedOrganizations = [];

            foreach ($organizationsId as $orgId) {
                foreach ($organizationsList as $organization) {
                    if ($organization->id == $orgId) {
                        array_push($appliedOrganizations, $organization);
                    }
                }
            }

            // $visits = array_combine($sentApplications, $appliedOrganizations);

            // var_dump($organizationsId);
            // var_dump($applicationsAbout, $applicationsDescription, $appliedOrganizations, $cities);

            return view('home', compact('user',
                'sentApplications', 'appliedOrganizations'));
        } else
            return abort(403, 'Neturite prieigos');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response|void
     *
     * public function index()
     * {
     * if (Gate::allows('admin-actions')) {
     * $users = User::orderBy('id', 'desc')->paginate(15);
     * return view('users.list', compact('users'));
     * } else
     * return abort(403, 'You are not authorized');
     * }
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response|void
     */
    public function create()
    {
        if (Gate::allows('admin-actions')) {
            return view('users.create');
        } else
            return abort(403, 'You are not authorized for this action');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|Response|void
     */
    public function edit($id)
    {
        if (Gate::allows('admin-actions')) {
            $user = User::findOrFail($id);
            return view('users.edit', compact('user'));
        } else
            return abort(403, 'You are not authorized for this action');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * @param User $user
     * @return Application|RedirectResponse|Redirector
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();


        return redirect('/')
            ->with('alert', 'Jūsų paskyra sėkmingai panaikinta. Ačiū, kad naudojotės Vizitavimo sistema.');

        /*
        if (Gate::allows('admin-actions')) {
            $user = User::findOrFail($id);
            $user->delete();
            return redirect('/')
                ->with('alert', 'Jūsų paskyra sėkmingai panaikinta. Ačiū, kad naudojotės Vizitavimo sistema.');
        } else
            return abort(403, 'Jūs neturite prieigos atlikti šiems veiksmams.');
        */
    }

    /**
     * @return Application|Factory|View|void
     * @throws \Exception
     */
    public function deleteUser()
    {
        if (Auth::guard('web')->check()) {
            $user = User::findOrFail(Auth::guard('web')->user()->id);
            Auth::guard('web')->logout();
            $user->delete();
            return view('auth.goodbyeUser');
        } else
            return abort(403, 'Neturite prieigos');
    }

    public function informationPage()
    {
        return view('navigation.usersIntro');
    }

    public function adminPanel()
    {
        if (Gate::allows('admin-actions'))
            return view('admin.panel');
    }
}
