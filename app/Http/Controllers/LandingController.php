<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LandingController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Application|Factory|View|Response
     */
    public function __invoke(Request $request)
    {
        /*
        $organizations = Organization::all();
        $areas = [];
        $kinds = [];
        $cities = [];

        $i = 0;
        $j = 0;
        $z = 0;
        foreach ($organizations as $organization) {
            if (in_array($organization->area, $areas) == false){
                $areas[$i] = $organization->area;
                $i++;
            }
            if (in_array($organization->kind, $kinds) == false) {
                $kinds[$z] = $organization->kind;
                $z++;
            }
            if (in_array($organization->city, $cities) == false) {
                $cities[$j] = $organization->city;
                $j++;
            }
        }
        */

        if ($request->get('search') == 1) {

            $queryBuilder = Organization::query();
            $request->validate([
                'city' => 'string|min:3|max:22|nullable',
                'area' => 'string|min:3|max:88|nullable',
                'kind' => 'string|min:6|max:22|nullable',
            ]);

            if ($request->has('area') && $request->get('area') != null) {
                $area = $request->get('area');
                $queryBuilder = $queryBuilder->where('area', $area);
            } else {
                $area = "";
            }
            if ($request->has('city') && $request->get('city') != null) {
                $city = $request->get('city');
                $queryBuilder = $queryBuilder->where('city', $city);
            } else {
                $city = "";
            }
            if ($request->has('kind') && $request->get('kind') != null) {
                $kind = $request->get('kind');
                $queryBuilder = $queryBuilder->where('kind', $kind);
            } else {
                $kind = "";
            }

            $organizations = $queryBuilder->get()->toArray();
        } else {
            $organizations = Organization::all()->toArray();
            $kind = "";
            $city = "";
            $area = "";
        }

        $dropdownOrgs = Organization::all()->toArray();
        $areas = [];
        $kinds = [];
        $cities = [];

        $i = 0;
        $j = 0;
        $z = 0;
        foreach ($dropdownOrgs as $organization) {
            if (in_array($organization['area'], $areas) == false) {
                $areas[$i] = $organization['area'];
                $i++;
            }
            if (in_array($organization['kind'], $kinds) == false) {
                $kinds[$z] = $organization['kind'];
                $z++;
            }
            if (in_array($organization['city'], $cities) == false) {
                $cities[$j] = $organization['city'];
                $j++;
            }
        }
        // dd($areas);
        return view('main', compact('organizations', 'areas', 'kinds', 'cities', 'kind', 'city', 'area'));
    }

}
