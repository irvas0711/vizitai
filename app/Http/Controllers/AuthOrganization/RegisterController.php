<?php

namespace App\Http\Controllers\AuthOrganization;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\Organization;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'organizations\login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:organization');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users',
                'regex:/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.(lm).(lt)$/'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        // regex:/^[A-Za-z0-9\.]*@(example)[.](com)$/
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Organization|Model
     */
    protected function create(array $data)
    {
        return Organization::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return Application|Factory|\Illuminate\Contracts\View\View|View
     */
    public function showRegistrationForm()
    {
        return view('organizations.register');
    }
}
