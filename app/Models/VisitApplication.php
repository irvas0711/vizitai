<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * App\Models\VisitApplication
 *
 * @property int $id
 * @property string $about
 * @property int $model_id
 * @property string $model
 * @property int $organization_id
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication newModelQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication newQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication query()
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication whereAbout($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication whereCreatedAt($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication whereDescription($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication whereId($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication whereModel($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication whereModelId($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication whereOrganizationId($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read Model|\Eloquent $applicable
 * @property-read \App\Models\Organization $organization
 */
class VisitApplication extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'about',
        'applicable_id',
        'applicable',
        'organization_id',
        'description',
    ];

    /**
     * @return BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * @return MorphTo
     */
    public function applicable()
    {
        return $this->morphTo();
    }
}
