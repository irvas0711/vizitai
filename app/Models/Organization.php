<?php

namespace App\Models;

use App\Notifications\MailResetPasswordToken;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\Organization
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $password
 * @property string|null $city
 * @property string|null $area
 * @property string|null $registration_picture
 * @property string|null $web_link
 * @property string|null $about
 * @property string|null $good_examples
 * @property string|null $kind
 * @property string|null $classes
 * @property int|null $application_sum
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization newModelQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization newQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization query()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization searchArea($area)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization searchCity($city)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization searchKind($kind)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereAbout($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereApplicationSum($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereArea($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereCity($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereClasses($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereCreatedAt($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereEmail($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereEmailVerifiedAt($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereGoodExamples($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereId($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereKind($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereName($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization wherePassword($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereRegistrationPicture($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereRememberToken($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereUpdatedAt($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereWebLink($value)
 * @mixin \Eloquent
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization isGymnasium()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization isMidst()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization isPreschool()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization isPrimary()
 * @property int $preschool
 * @property int $primary
 * @property int $midst
 * @property int $gymnasium
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization searchIsGymnasium()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization searchIsMidst()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization searchIsPreschool()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization searchIsPrimary()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereGymnasium($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization whereMidst($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization wherePreschool($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization wherePrimary($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VisitApplication[] $applications
 * @property-read int|null $applications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VisitApplication[] $organizationApplications
 * @property-read int|null $organization_applications_count
 */
class Organization extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'city',
        'area',
        'registration_picture',
        'web_link',
        'about',
        'good_examples',
        'kind',
        'classes',
        'preschool',
        'primary',
        'midst',
        'gymnasium',
        'application_sum',
        //   'vote_sum',
        //   'vote_quantity'
    ];


    /**
     * PAKOPINAU IŠ User.php
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /*
     * @param $query
     * @param $area
     * @return mixed
     */
    public function scopeSearchArea($query, $area)
    {
        return $query->where('area', $area);
    }

    /*
     * @param $query
     * @param $city
     * @return mixed
     */
    public function scopeSearchCity($query, $city)
    {
        return $query->where('city', $city);
    }

    /*
     * @param $query
     * @param $kind
     * @return mixed
     */
    public function scopeSearchKind($query, $kind)
    {
        return $query->where('kind', $kind);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSearchIsPreschool($query)
    {
        return $query->where('preschool', 1);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSearchIsPrimary($query)
    {
        return $query->where('primary', 1);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSearchIsMidst($query)
    {
        return $query->where('midst', 1);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSearchIsGymnasium($query)
    {
        return $query->where('gymnasium', 1);
    }

    /**
     * @return MorphMany
     */
    public function organizationApplications()
    {
        return $this->morphMany(VisitApplication::class, 'applicable');
    }

    /**
     * @return HasMany
     */
    public function applications()
    {
        return $this->hasMany(VisitApplication::class);
    }

    /**
     * Send a password reset notification to the user.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $url = 'http://20.39.233.237/organizations/password/reset/'.$token;
        // 127.0.0.1
        $this->notify(new MailResetPasswordToken($url));
    }
}
