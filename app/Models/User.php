<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string|null $surname
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string $type
 * @property string|null $kind
 * @property string|null $organization
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method navigation \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|User query()
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereKind($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereOrganization($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereSurname($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereType($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable // implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'type',
        'kind',
        'organization'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
