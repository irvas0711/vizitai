<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Area
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method navigation \Illuminate\Database\Eloquent\Builder|Area newModelQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Area newQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Area query()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Area whereCreatedAt($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Area whereId($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Area whereName($value)
 * @method navigation \Illuminate\Database\Eloquent\Builder|Area whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Area extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
