<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('admin-actions', function ($user){
            return $user->type=='administrator';
        });
        Gate::define('logged-in-user', function ($user){
            return Auth::check();
        });
        Gate::define('logged-in-organization', function ($organization){
            return Auth::guard('organization')->check();
        });
    }
}
