## About the system

Vizitavimo sistema or Viziting system is a platform which helps to find a unique good practices of educational institutions and contact with them by sending visiting request for real visit. Visits are dedicated to share those unique good practices in detail with real experience. Educational institutions consist of schools and youth centers.


## User roles

The project includes 3 types of users - organizations, regular user, administrator. Try these logins for each user type.

Švietimo įstaigoms/Organizations:

e-mail: info@dm.lt, password: 123456789

Vizituojantiems vartotojams/Regular users:

e-mail: jonas@gmail.com, password: Pas1234*

Administratorius/Administrator:

e-mail: irvas0711@gmail.com, password: Pas1234*

Administrator uses the same login form as regular users. 

## Click for demonstration

[Demonstration](https://vizitai.0z.lt/)
