@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1 class="display-6 my-3" style="font-weight: bolder">Mano profilis</h1>
            </div>
            <div class="col-6">

            </div>
            <hr class="my-3">
        </div>


        @if ($message = Session::get('success'))
            <div class="alert alert-success text-center">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="form-group row input-group-lg">
                        <h3 class="display-6 my-1" style="font-weight: bolder">Duomenys</h3>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label font-weight-bold">Vardas:</label>
                        <div class="col">
                            <p class="form-control-plaintext">{{ $user->name.' '.$user->surname }}</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label font-weight-bold">El. paštas:</label>
                        <div class="col">
                            <p class="form-control-plaintext">{{ $user->email}}</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="form-group row input-group-lg">
                            <div class="col text-center">
                                <a href="{{ url('show-organizations') }}" class="btn btn-primary">Švietimo
                                    įstaigų
                                    paieška</a>
                            </div>
                            <form action="{{ route('user.delete') }}" method="get" style="margin-left: 25px">
                                <button type="submit" class="btn btn-outline-danger">Ištrinti paskyrą</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group row">
                        <h3 class="display-6 my-1" style="font-weight: bolder">Pateiktos vizitavimo užklausos</h3>
                    </div>
                    <div class="form-group row">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center" scope="col">Nr</th>
                                <th class="text-center" scope="col">Švietimo įstaiga</th>
                                <th class="text-center" scope="col">Gyvenvietė</th>
                                <th class="text-center" scope="col">Mano prisistatymas</th>
                                <th class="text-center" scope="col">Žinutė švietimo įstaigai</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($appliedOrganizations as $organization)
                                {{-- {{ $sentApplications[$loop->iteration - 1]->about }} --}}
                                <tr class="text-center">
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $organization->name }}</td>
                                    <td>{{ $organization->city }}</td>
                                    <td>{{ $sentApplications[$loop->iteration - 1]->about }}
                                        {{--
                                        <button type="button" class="btn btn-outline-success btn-sm"
                                                data-toggle="modal"
                                                data-target="#exampleModal">
                                            Peržiūrėti
                                        </button>
                                        --}}
                                    </td>
                                    <td>{{ $sentApplications[$loop->iteration - 1]->description }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{--    @csrf
                        @method('DELETE')
                        <div class="col-12 text-right" style="margin-top: 65px">
                            <button type="submit" title="delete" class="btn btn-danger btn-sm">
                                Ištrinti paskyrą
                            </button>
                        </div>

                    </div>
                </form>
    {{--
<div class="col-md-6">
    <div class="card">
        <div class="card-header">{{ __('Sveikiname prisijungus!') }}</div>


        <div class="card-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-center">
                    <p>{{ $message }}</p>
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card-body">
                <form action="" method="POST">
                    <div class="form-group row">
                        <div class="col-12 text-center">
                            <a href="{{ url('show-organizations') }}" class="btn btn-outline-info btn-lg">Švietimo
                                įstaigų
                                paieška</a>
                        </div>

                {{--    @csrf
                        @method('DELETE')
                        <div class="col-12 text-right" style="margin-top: 65px">
                            <button type="submit" title="delete" class="btn btn-danger btn-sm">
                                Ištrinti paskyrą
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
--}}
            </div>
        </div>
    </div>
@endsection
