@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 py-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="display-6 my-1" style="font-weight: bolder">Mano profilis</h3>
                    </div>

                    <div class="card-body">
                        <div class="card-body">
                            <div class="form-group row form-inline">
                                <div class="col-4">
                                    <a href="{{ url('show-organizations') }}" class="btn btn-primary">Švietimo įstaigų
                                        paieška</a>
                                </div>
                                <div class="col-4 text-center">
                                    <a href="{{ url('organization/applications') }}" class="btn btn-success">Vizitavimo
                                        užklausos</a>
                                </div>

                                <div class="col-4 text-right">
                                    <form action="{{ route('organization.delete') }}" method="get">
                                        <button type="submit" class="btn btn-outline-danger">Ištrinti paskyrą</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <div class="card-body">
                            <h3 class="display-6 my-1" style="font-weight: bolder">Duomenys</h3>
                            <hr class="my-3">
                            @if($organization['city'] == null || $organization['area'] == null ||
                                $organization['kind'] == null || $organization['about'] == null ||
                                $organization['good_examples'] == null)
                                <div class="alert alert-info text-center" role="alert">
                                    <p>Sveikiname prisijungus! Norėdami savo švietimo įstaigą matyti paieškoje kviečiame
                                        baigti savo profilio kūrimą.</p>
                                </div>
                            @endif
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success text-center">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="staticName" class="col-4 col-form-label text-right font-weight-bold">Jūsų
                                    įstaigos
                                    pavadinimas</label>
                                <div class="col">
                                    <textarea rows="1" type="text" readonly class="form-control-plaintext"
                                              id="staticName">{{ $organization->name }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-4 col-form-label text-right font-weight-bold">Elektroninis
                                    paštas</label>
                                <div class="col">
                                    <textarea rows="1" type="text" readonly class="form-control-plaintext"
                                              id="staticEmail">{{ $organization->email }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticCity"
                                       class="col-4 col-form-label text-right font-weight-bold">Miestas/Gyvenvietė</label>
                                <div class="col">
                                    <textarea rows="1" type="text" readonly class="form-control-plaintext"
                                              id="staticCity">{{ $organization->city }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticArea" class="col-4 col-form-label text-right font-weight-bold">Savivaldybė</label>
                                <div class="col">
                                    <textarea rows="1" type="text" readonly class="form-control-plaintext"
                                              id="staticArea">{{ $organization->area }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticKind" class="col-4 col-form-label text-right font-weight-bold">Įstaigos
                                    tipas</label>
                                <div class="col">
                                    <textarea rows="1" type="text" readonly class="form-control-plaintext"
                                              id="staticKind">{{ $organization->kind }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="staticClasses" class="col-4 col-form-label text-right font-weight-bold">Ugdomos
                                    klasės</label>
                                <div class="col-6 py-2">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="preschool" id="preschool"
                                               value="{{ $organization->preschool }}"
                                               {{($organization->preschool == 1)?'checked':''}} disabled>
                                        <label class="form-check-label" for="preschool">Priešmokyklinės</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="primary" id="primary"
                                               value="{{ $organization->primary }}"
                                               {{($organization->primary == 1)?'checked':''}} disabled>
                                        <label class="form-check-label" for="primary">Pradinės</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="midst" id="midst"
                                               value="{{ $organization->midst }}"
                                               {{($organization->midst == 1)?'checked':''}} disabled>
                                        <label class="form-check-label" for="midst">5-8 kl.</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="gymnasium" id="gymnasium"
                                               value="{{ $organization->gymnasium }}"
                                               {{($organization->gymnasium == 1)?'checked':''}} disabled>
                                        <label class="form-check-label" for="gymnasium">Gimnazinės</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticAbout" class="col-4 col-form-label text-right font-weight-bold">Prisistatymas</label>
                                <div class="col">
                                    <textarea cols="40" rows="8" type="text" readonly class="form-control-plaintext"
                                              id="staticAbout">{{ $organization->about }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticGoodExamples"
                                       class="col-4 col-form-label text-right font-weight-bold">Geroji
                                    patirtis</label>
                                <div class="col">
                                    <textarea cols="40" rows="12" type="text" readonly class="form-control-plaintext"
                                              id="staticGoodExamples">{{ $organization->good_examples }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row mb-12">
                                <a class="btn btn-primary col-md-6 offset-md-5" href="{{ route('organization.edit') }}"
                                   title="Sukurti naują švietimo įstaigą">
                                    Redaguoti
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
