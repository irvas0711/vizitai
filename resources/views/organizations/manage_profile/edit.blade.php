@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 py-4">
                <div class="card">
                    <div class="card-header text-center">{{ __('Redaguoti švietimo įstaigos duomenis') }}</div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Dėmesio.</strong> Patikrinkite įvestį, galbūt įsivėlė klaida?<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body">
                        <form action="{{ route('organization.update', $organization->id) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Švietimo įstaigos pavadinimas') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ $organization->name }}" required autocomplete="email" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Elektroninis paštas') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ $organization->email }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Miestas/Gyvenvietė') }}</label>

                                <div class="col-md-6">
                                    <input id="city" type="text"
                                           class="form-control @error('city') is-invalid @enderror" name="city"
                                           value="{{ $organization->city }}" required autocomplete="city" autofocus>

                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="area"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Savivaldybė') }}</label>

                                <div class="col-md-6">
                                    <select name="area" id="area" class="selectpicker form-control"
                                            data-live-search="true"
                                            type="text" data-style="btn-light" required>
                                        <option value="">- Pasirinkite savivaldybę -</option>
                                        @foreach($areas as $area)
                                            @if($current_area == $area->name)
                                                <option selected="selected">{{ $area->name }}</option>
                                            @else
                                                <option>{{ $area->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @error('area')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kind"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Įstaigos tipas') }}</label>

                                <div class="col-md-6">
                                    <select name="kind" id="kind" class="selectpicker form-control
                                            @error('kind') is-invalid @enderror"
                                            data-live-search="true"
                                            type="text" data-style="btn-light">
                                        <option value="">- Pasirinkite tipą -</option>
                                        @foreach($allKinds as $kind)
                                            @if($current_kind == $kind)
                                                <option selected="selected">{{ $kind }}</option>
                                            @else
                                                <option>{{ $kind }}</option>
                                            @endif
                                        @endforeach
                                    </select>
<!--
                                    <input id="kind" type="text"
                                           class="form-control" name="kind"
                                           value="Valstybinė" hidden>
-->
                                    @error('kind')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="classes"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Ugdomos klasės') }}</label>

                                <div class="col-6 py-2">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="preschool"
                                               id="preschool" value="{{ $organization->preschool }}"
                                            {{($organization->preschool == 1)?'checked':''}}>
                                        <label class="form-check-label" for="preschool">Priešmokyklinės</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="primary" id="primary"
                                               value="{{ $organization->primary }}"
                                            {{($organization->primary == 1)?'checked':''}}>
                                        <label class="form-check-label" for="primary">Pradinės</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="midst" id="midst"
                                               value="{{ $organization->midst }}"
                                            {{($organization->midst == 1)?'checked':''}}>
                                        <label class="form-check-label" for="midst">5-8 kl.</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="gymnasium"
                                               id="gymnasium" value="{{ $organization->gymnasium }}"
                                            {{($organization->gymnasium == 1)?'checked':''}}>
                                        <label class="form-check-label" for="gymnasium">Gimnazinės</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="about"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Prisistatymas') }}</label>

                                <div class="col-md-6">
                                    <textarea rows="8" id="about" type="text"
                                              class="form-control @error('about') is-invalid @enderror" name="about"
                                              required autocomplete="about"
                                              autofocus>{{ $organization->about }}</textarea>

                                    @error('about')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="good_examples"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Geroji patirtis') }}</label>

                                <div class="col-md-6">
                                    <textarea rows="10" id="good_examples" type="text"
                                              class="form-control @error('good_examples') is-invalid @enderror"
                                              name="good_examples" required autocomplete="good_examples"
                                              autofocus>{{ $organization->good_examples }}</textarea>

                                    @error('good_examples')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row mb-12">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-success col-md-9">
                                        {{ __('Išsaugoti') }}
                                    </button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12 margin-tb">
                                    <div class="pull-right">
                                        <a class="btn btn-outline-info" href="{{ route('organization.profile') }}"
                                           title="">Grįžti</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
