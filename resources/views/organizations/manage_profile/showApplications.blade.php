@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="display-6 my-3" style="font-weight: bolder">Vizitavimo užklausos</h1>
        <hr class="my-3">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-center">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="container">
            <div class="row">
                <div class="col-12 my-3">
                    <div class="form-group row">
                        <h3 class="display-6 my-1" style="font-weight: bolder">Gautos vizitavimo užklausos</h3>
                    </div>
                    <div class="form-group row">
                        <h5 class="display-6 my-1" style="font-weight: bolder">Nuo švietimo įstaigų</h5>
                    </div>
                    <div class="form-group row">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center" scope="col">Nr</th>
                                <th class="text-center" scope="col">Švietimo įstaiga</th>
                                <th class="text-center" scope="col">Gyvenvietė</th>
                                <th class="text-center" scope="col">Tipas</th>
                                <th class="text-center" scope="col">El. paštas</th>
                                <th class="text-center" scope="col">Prisistatymas</th>
                                <th class="text-center" scope="col">Pasiūlymai vizitui ir lūkesčiai</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($waitingOrganizations as $org)
                                <tr class="text-center">
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $org->name }}</td>
                                    <td>{{ $org->city }}</td>
                                    <td>{{ $org->kind }}</td>
                                    <td>{{ $org->email }}</td>
                                    <td>{{ $receivedApplicationsFromOrganizations[$loop->iteration - 1]->about }}
                                        {{--
                                        <button type="button" class="btn btn-outline-success btn-sm"
                                                data-toggle="modal"
                                                data-target="#exampleModal">
                                            Peržiūrėti
                                        </button>
                                        --}}
                                    </td>
                                    <td>{{ $receivedApplicationsFromOrganizations[$loop->iteration - 1]->description }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group row">
                        <h5 class="display-6 my-1" style="font-weight: bolder">Nuo pavienių vartotojų</h5>
                    </div>
                    <div class="form-group row">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center" scope="col">Nr</th>
                                <th class="text-center" scope="col">Vizituotojo vardas</th>
                                <th class="text-center" scope="col">El. paštas</th>
                                <th class="text-center" scope="col">Prisistatymas</th>
                                <th class="text-center" scope="col">Pasiūlymai vizitui ir lūkesčiai</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($waitingUsers as $user)
                                {{-- {{ $sentApplications[$loop->iteration - 1]->about }} --}}
                                <tr class="text-center">
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $user->name.' '.$user->surname }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $receivedApplicationsFromUsers[$loop->iteration - 1]->about }}
                                        {{--
                                        <button type="button" class="btn btn-outline-success btn-sm"
                                                data-toggle="modal"
                                                data-target="#exampleModal">
                                            Peržiūrėti
                                        </button>
                                        --}}
                                    </td>
                                    <td>{{ $receivedApplicationsFromUsers[$loop->iteration - 1]->description }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12 my-3">
                    <div class="form-group row">
                        <h3 class="display-6 my-1" style="font-weight: bolder">Pateiktos vizitavimo užklausos</h3>
                    </div>
                    <div class="form-group row">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center" scope="col">Nr</th>
                                <th class="text-center" scope="col">Švietimo įstaiga</th>
                                <th class="text-center" scope="col">Gyvenvietė</th>
                                <th class="text-center" scope="col">Mano prisistatymas</th>
                                <th class="text-center" scope="col">Pasiūlymai vizitui ir lūkesčiai</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($appliedOrganizations as $organization)
                                {{-- {{ $sentApplications[$loop->iteration - 1]->about }} --}}
                                <tr class="text-center">
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $organization->name }}</td>
                                    <td>{{ $organization->city }}</td>
                                    <td>{{ $sentApplications[$loop->iteration - 1]->about }}
                                        {{--
                                        <button type="button" class="btn btn-outline-success btn-sm"
                                                data-toggle="modal"
                                                data-target="#exampleModal">
                                            Peržiūrėti
                                        </button>
                                        --}}
                                    </td>
                                    <td>{{ $sentApplications[$loop->iteration - 1]->description }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
