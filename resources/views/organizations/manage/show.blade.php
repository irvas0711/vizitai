@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 py-3">
                <div class="card">
                    <div class="card-header text-center">{{ __('Švietimo įstaigos duomenys') }}</div>

                    <div class="card-body">
                        <div class="form-group row">
                            <label for="staticName" class="col-4 col-form-label text-right" style="font-weight: bold">Pavadinimas</label>
                            <div class="col">
                                <input type="text" readonly class="form-control-plaintext" id="staticName" value={{ $organization->name }}>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-4 col-form-label text-right" style="font-weight: bold">Elektroninis paštas</label>
                            <div class="col">
                                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value={{ $organization->email }}>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticDate" class="col-4 col-form-label text-right" style="font-weight: bold">Sukūrimo data</label>
                            <div class="col">
                                <input type="text" readonly class="form-control-plaintext" id="staticDate" value={{ $organization->created_at }}>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 margin-tb">
                                <div class="pull-right">
                                    <a class="btn btn-outline-info" href="{{ route('organizations.index') }}" title="Go back"> <i class="fas fa-backward "></i>Grįžti</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 py-xl-5">

            </div>
            <div class="col-md-8 p-3">

            </div>
        </div>
    </div>
@endsection
