@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        <h1 class="display-6 my-3" style="font-weight: bolder">Švietimo įstaigų valdymas</h1>
        <hr class="my-3">
        <!--
            <div style="margin-top: 42px">
                <h3 class="display-8 my-3" style="font-weight: bolder; margin-bottom: 160px">Patvirtinti registracijas</h3>

                TABLE FOR CONFIRMATION
                <form method="POST" class="text-lg-center">
                    <div class="align-self-sm-center" style="margin-top: 15px; text-align: center">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Įstaiga</th>
                                <th scope="col">Tipas</th>
                                <th scope="col">El. paštas</th>
                                <th scope="col">Mokytojo pažymėjimas</th>
                                <th scope="col">Nuoroda</th>
                                <th scope="col">Patvirtinti</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">2</th>
                                <td >Juliaus Jonavičiaus Sargybos mokykla</td>
                                <td>Karinė</td>
                                <td>info@sargyba.lt</td>
                                <td>Nuotrauka</td>
                                <td>
                                    <a href="">https://getbootstrap.com/docs/4.2</a>
                                </td>
                                <td>
                                    <form class="btn-group-sm">
                                        <button class="btn btn-success my-2 my-sm-0" type="submit" >Patvirtinti</button>
                                        <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Atšaukti</button>
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td >Juliaus Jonavičiaus Sargybos mokykla</td>
                                <td>Karinė</td>
                                <td>info@sargyba.lt</td>
                                <td>Nuotrauka</td>
                                <td>
                                    <a href="">https://getbootstrap.com/docs/4.2</a>
                                </td>
                                <td>
                                    <form class="btn-group-sm">
                                        <button class="btn btn-success my-2 my-sm-0" type="submit" >Patvirtinti</button>
                                        <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Atšaukti</button>
                                    </form>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        -->
        <!-- Organizations CRUD  -->
        <div style="margin-top: 42px">
            <div class="row" style="margin-bottom: 24px">
                <div class="col-4">
                    <h3 class="text-left" style="font-weight: bolder">Naujų įstaigų registracija</h3>
                </div>
                <div class="col-4">
                </div>
                <div class="col-4 text-right">
                    <a class="btn btn-info align-r" href="{{ route('organizations.create') }}"
                       title="Sukurti naują švietimo įstaigą">
                        Pridėti švietimo įstaigą
                    </a>
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success text-center">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="align-self-sm-center" style="margin-top: 15px; text-align: center">
                <table class="table table-striped">
                    <tr>
                        <th scope="col">Nr</th>
                        <th scope="col">Įstaigos pavadinimas</th>
                        <th scope="col">Elektroninis paštas</th>
                        <th width="280px">Valdymas</th>
                        <th scope="col">Siųsti registracijos nuorodą</th>
                    </tr>
                    @foreach ($organizations as $organization)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $organization->name }}</td>
                            <td>{{ $organization->email }}</td>
                            <td>
                                <form action="{{ route('organizations.destroy', $organization->id) }}" method="POST">

                                    <a href="{{ route('organizations.show', $organization->id) }}" title="show">
                                        <i class="text-success">Peržiūrėti</i>
                                    </a>

                                    <a href="{{ route('organizations.edit', $organization->id) }}" title="edit">
                                        <i class="">Redaguoti</i>

                                    </a>

                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" title="delete"
                                            style="border: none; background-color:transparent;">
                                        <i class="text-danger">Ištrinti</i>
                                    </button>
                                </form>
                            </td>
                            <td>
                                <form action="{{ route('organization.password.email') }}" method="POST">
                                    @csrf
                                    <input name="email" id="email" type="hidden" value="{{ $organization->email }}">
                                    <div>
                                        <button class="btn btn-primary" type="submit" name="invite">
                                            Siųsti
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                {{ $organizations->links() }}
            </div>
        </div>
    </div>
@endsection
