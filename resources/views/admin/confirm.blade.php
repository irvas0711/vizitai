@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        <h1 class="display-6 my-3" style="font-weight: bolder">Švietimo įstaigų registracijos užklausos</h1>
        <hr class="my-3">
        <h3 class="text-left" style="font-weight: bolder">Siųsti pakvietimus</h3>
        <form method="POST" class="mr-sm-2">
            <div class="input-group mr-sm-2">
                <input type="text" class="form-control col-4" placeholder="Elektroninis paštas" aria-label="Email" aria-describedby="basic-addon1">
                <span class=""></span>
                <button class="btn btn-success col-1" style="margin-left: 8px" type="submit" >Siųsti</button>
            </div>
        </form>

        <hr class="my-3">
        <h3 class="display-8 my-3" style="font-weight: bolder">Patvirtinimui</h3>

        <!-- TABLE FOR CONFIRMATION -->
        <form method="POST" class="text-lg-center">
            <div class="align-self-sm-center" style="margin-top: 15px; text-align: center">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Įstaiga</th>
                        <th scope="col">Tipas</th>
                        <th scope="col">El. paštas</th>
                        <th scope="col">Mokytojo pažymėjimas</th>
                        <th scope="col">Nuoroda</th>
                        <th scope="col">Patvirtinti</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">2</th>
                        <td >Juliaus Jonavičiaus Sargybos mokykla</td>
                        <td>Karinė</td>
                        <td>info@sargyba.lt</td>
                        <td>Nuotrauka</td>
                        <td>
                            <a href="">https://getbootstrap.com/docs/4.2</a>
                        </td>
                        <td>
                            <form class="btn-group-sm">
                                <button class="btn btn-success my-2 my-sm-0" type="submit" >Patvirtinti</button>
                                <button class="btn btn-danger my-2 my-sm-0" type="submit">Atšaukti</button>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td >Juliaus Jonavičiaus Sargybos mokykla</td>
                        <td>Karinė</td>
                        <td>info@sargyba.lt</td>
                        <td>Nuotrauka</td>
                        <td>
                            <a href="">https://getbootstrap.com/docs/4.2</a>
                        </td>
                        <td>
                            <form class="btn-group-sm">
                                <button class="btn btn-success my-2 my-sm-0" type="submit" >Patvirtinti</button>
                                <button class="btn btn-danger my-2 my-sm-0" type="submit">Atšaukti</button>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
@endsection
