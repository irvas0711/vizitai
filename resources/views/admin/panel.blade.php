@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        <div class="row">
            <a href="{{ url('/organizations') }}">
                <div class="col-md-6 mx-1 my-1 p-0">
                    Organizacijų valdymas
                </div>
            </a>

            <a>
                <div class="col-md-6 mx-1 my-1 p-0">
                    Vartotojų skaldymas
                </div>
            </a>

            <a>
                <div class="col-md-6 mx-1 my-1 p-0">
                    Užklausų valdymas skaldymas
                </div>
            </a>

            <a>
                <div class="col-md-6 mx-1 my-1 p-0">
                    Knygų valdymas
                </div>
            </a>
        </div>
    </div>

@endsection
