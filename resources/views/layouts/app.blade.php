<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!--BS Scripts and sheets-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
            integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <!-- Styles -->
    <link href="{{asset('/css/app.css')}}" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="{{asset('/js/bootstrap-select.min.js')}}"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="{{asset('/js/i18n/defaults-lt_LT.min.js')}}"></script>

    <!--
        <link rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

         Fonts
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        Styles
        <link href=" asset('css/app.css') " rel="stylesheet">

        Scripts
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"
                integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>

        <script src=" asset('js/app.js') }}" defer></script>
        -->
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('organizations.intro') }}">{{ __('Švietimo įstaigoms') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('users.intro') }}">{{ __('Vizituojantiems') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('organizations.statistics') }}">{{ __('Statistika') }}</a>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @if(Auth::guard('web')->check())
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                            <!-- Auth::guard('organization')->user()->name -->
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('home') }}">
                                    {{ __('Profilis') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Atsijungti') }}
                                </a>

                                @if(Auth::user()->type == 'administrator')
                                    <a href="{{ url('/organizations') }}" class="dropdown-item">Valdymo skydelis</a>
                                @endif

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>

                    @elseif(Auth::guard('organization')->check())
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::guard('organization')->user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('organization.profile') }}">
                                    {{ __('Profilis') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Atsijungti') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link"></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"></a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>


    <main class="pb-lg-5">
        {{--
        @if(session("success"))
            <div class="alert alert-success text-center">
                <p>{{ session("success") }}</p>
            </div>
        @endif
    --}}
        @if(session("info"))
            <div class="alert alert-success text-center">
                <p>{{ session("info") }}</p>
            </div>
        @endif
        @if(session("error"))
            <div class="alert alert-danger text-center">
                <p>{{ session("error") }}</p>
            </div>
            {{--
            <div class="container-fluid">
                <div class="container mx-auto">
                    <div class="alert-danger">
                        {{ session("error") }}
                    </div>
                </div>
            </div>
            --}}
        @endif
        @yield('content')
    </main>

    <footer class="text-muted py-3">
        <hr class="my-3">
        <div class="container">
            <p class="float-right">
                <a href="#">Į viršų</a>
            </p>
            <p>© 2020 Švietimo įstaigų vizitavimo sistema</p>
        </div>
    </footer>


    <!--
    <footer class="text-muted">
        <hr class="my-3">
        <div class="container">
            <p class="float-right">
                <a href="#">Į viršų</a>
            </p>
            <p>© Švietimo įstaigų vizitavimo sistema</p>
        </div>
    </footer>

    <footer class="text-muted card-footer">
        <div class="footer-copyright text-center">
            <p>© 2020 Švietimo įstaigų vizitavimo sistema</p>
        </div>
    </footer>

    <footer class="card-footer">
        <div class="footer-copyright text-center py-3">
            © 2020 Viztavimo sistema
        </div>
    </footer>
     -->

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
</body>
</html>
