<!DOCTYPE html>
<html>
<head>
    <title>ItsolutionStuff.com</title>
</head>
<body>
<p>Laba diena,</p>
<p>Linkėjimai nuo Viztavimo sistemos komandas!</p>
<p>Jūs gavote naują vizitavimo užklausą. Jei jus sudomino šis vizito pasiūlymas, atsakykite į jį
elektroniniu paštu, kurį gavote.</p>
<p><b>Vizitavimo užklausa nuo:</b></p>
<p>{{ $visitor->name}} {{ $visitor->surname}}</p>
<p><b>Elektroninis paštas:</b></p>
<p>{{ $visitor->email }}</p>
<p><b>Prisistatymas:</b></p>
<p>{{ $visitApplication->about }}</p>
<p><b>Vizito vizija ir pasiūlymai:</b></p>
<p>{{ $visitApplication->description }}</p><br>
<p>Linkime gerų vizitų ir dalinimosi gerosiomis patirtimis švietimo ateičiai kurti.</p>
<p><i>Viztavimo sistemos komanda</i></p>
</body>
</html>


