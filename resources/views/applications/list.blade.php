@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        <h1 class="display-6 my-3" style="text-align: center; font-weight: bolder">Švietimo įstaigų paieška</h1>
        <!-- SEARCH BOTTONS -->
        <form method="POST" class="text-lg-center">
            <div class="btn-group mr-sm-2" style="margin-top: 15px">

                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle form-inline mr-sm-2" style="background-color: dodgerblue" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Švietimo įstaigos tipas
                    </button>

                    <select name="kind" id="kind" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <option class="dropdown-item" >Valstybinė mokykla</option>
                        <option class="dropdown-item" >Privati mokykla</option>
                        <option class="dropdown-item" >Alternatyvaus švietimo mokykla</option>
                        <option class="dropdown-item" >Jaunimo centras</option>
                    </select>
                </div>

                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle form-inline mr-sm-2" style="background-color: dodgerblue" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Savivaldybė
                    </button>
                    <select name="area" id="area" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <option class="dropdown-item" >Vilniaus</option>
                        <option class="dropdown-item" >Kauno</option>
                        <option class="dropdown-item" >Klaipėdos</option>
                        <option class="dropdown-item" >Druskininkų</option>
                    </select>
                </div>

                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle form-inline mr-sm-2" style="background-color: dodgerblue" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Miestas
                    </button>
                    <select name="city" id="city" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <option class="dropdown-item" >Vilniaus</option>
                        <option class="dropdown-item" >Kauno</option>
                        <option class="dropdown-item" >Klaipėdos</option>
                        <option class="dropdown-item" >Druskininkų</option>
                    </select>
                </div>

                <form class="form-inline my-md-5">
                    <button class="btn btn-secondary my-2 my-sm-0" type="submit" style="background-color: darkblue">Rodyti visas</button>
                </form>
            </div>

            <div class="text-lg-center" style="margin-top: 15px; font-style: normal">
                <div class="form-check form-check-inline">
                    <a style="font-weight: bold; margin-right: 8px">Klasės</a>
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                    <label class="form-check-label" for="inlineCheckbox1">Priešmokyklinės</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                    <label class="form-check-label" for="inlineCheckbox2">Pradinės</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                    <label class="form-check-label" for="inlineCheckbox2">5-8 kl.</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                    <label class="form-check-label" for="inlineCheckbox2">Gimnazinės</label>
                </div>
            </div>
        </form>

        <form method="POST" class="text-lg-center">
            <div class="align-self-sm-center" style="margin-top: 15px; text-align: center">
                <div class="container">
                    <!-- <input class="form-control" id="listSearch" type="text" placeholder="Type something to search list items"><br> -->
                    <hr class="my-4">
                    <ul class="list-group" id="myList">
                        <div class="list-group-item">
                            <div class="row justify-content-md-center">
                                <div style="font-size: 26px">
                                    <a href="">KTU Gimnazija</a>
                                </div>
                            </div>
                            <div class="row justify-content-md-center">
                                <div>Įstaigos tipas, Miestas, Savivaldybė</div>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="row justify-content-md-center">
                                <div style="font-size: 26px">
                                    <a href="">Klaipėdos Licėjus</a>
                                </div>
                            </div>
                            <div class="row justify-content-md-center">
                                <div>Įstaigos tipas, Miestas, Savivaldybė</div>
                            </div>
                        </div>
                    </ul>
                </div>

                <div class="container text-center mx-auto">
                    <nav class="align-self-sm-center" aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </form>
    </div>

@endsection
