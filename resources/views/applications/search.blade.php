@extends('layouts.app')

@section('content')
    <div class="container py-3">
        <h1 class="display-6" style="text-align: center; font-weight: bolder">Švietimo įstaigų paieška</h1>

        <!-- SEARCH DROPDOWNS -->
        <form>
            @csrf
            <div class="form-group">
                <div class="form-row justify-content-center my-3">
                    <div class="col-auto">
                        <label for="area"></label>
                        <select name="area" id="area" class="selectpicker @error('area') is-invalid @enderror"
                                data-live-search="true"
                                data-style="btn-primary">
                            <option value="" class="font-weight-bold">- Visos Savivaldybės -</option>
                            @foreach($areas as $area1)
                                @if(isset($area) && $area != null && $area == $area1)
                                    <option selected="selected">{{ $area1 }}</option>
                                @elseif($area1 != null)
                                    <option>{{ $area1 }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col-auto">
                        <label for="kind"></label>
                        <select name="kind" id="kind" class="selectpicker @error('kind') is-invalid @enderror"
                                data-live-search="true"
                                data-style="btn-primary">
                            <option value="" class="font-weight-bold">- Visi įstaigų tipai -</option>
                            @foreach($kinds as $kind1)
                                @if(isset($kind) && $kind != null && $kind == $kind1)
                                    <option selected="selected">{{ $kind1 }}</option>
                                @elseif($kind1 != null)
                                    <option>{{ $kind1 }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col-auto">
                        <label for="city"></label>
                        <select name="city" id="city" class="selectpicker @error('city') is-invalid @enderror"
                                data-live-search="true"
                                data-style="btn-primary">
                            <option value="" class="font-weight-bold">- Visi miestai -</option>
                            @foreach($cities as $city1)
                                @if(isset($city) && $city != null && $city == $city1)
                                    <option selected="selected">{{ $city1 }}</option>
                                @elseif($city1 != null)
                                    <option>{{ $city1 }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col-auto">
                        <button class="btn btn-secondary" type="submit"
                                style="background-color: darkblue">Ieškoti
                        </button>
                    </div>
                </div>

                <div class="form-row justify-content-center" style="margin-top: 15px; font-style: normal">
                    <div class="form-check form-check-inline">
                        <a style="font-weight: bold; margin-right: 8px">Klasės</a>
                        <input class="form-check-input" type="checkbox" name="preschool" id="preschool"
                               value="1" {{($preschool == 1)?'checked':''}}>
                        <label class="form-check-label" for="preschool">Priešmokyklinės</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="primary" id="primary"
                               value="1" {{($primary == 1)?'checked':''}}>
                        <label class="form-check-label" for="primary">Pradinės</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="midst" id="midst"
                               value="1" {{($midst == 1)?'checked':''}}>
                        <label class="form-check-label" for="midst">5-8 kl.</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="gymnasium" id="gymnasium"
                               value="1" {{($gymnasium == 1)?'checked':''}}>
                        <label class="form-check-label" for="gymnasium">Gimnazinės</label>
                    </div>
                </div>
            </div>
            <input type="hidden" name="search" value="1">
        </form>

        <div class="align-self-sm-center" style="margin-top: 15px; text-align: center">
            <div class="container">
                <!-- <input class="form-control" id="listSearch" type="text" placeholder="Type something to search list items"><br> -->
                <hr class="my-4">
                <ul class="list-group" id="myList">
                    @if($organizations == null)
                        <div class="alert alert-info text-center">
                            <p>Pagal nurodytus paieškos kriterijus švietimo įstaigų nėra.</p>
                        </div>
                    @endif
                    @foreach ($organizations as $organization)
                        @if($organization['city'] != null && $organization['area'] != null &&
                            $organization['kind'] != null && $organization['about'] != null &&
                            $organization['good_examples'] != null)
                            <div class="list-group-item">
                                <div class="row justify-content-md-center">
                                    <div style="font-size: 26px">
                                        <a href="{{ route('show', $organization['id']) }}">{{ $organization['name'] }}</a>
                                    </div>
                                </div>
                                <div class="row justify-content-md-center">
                                    <a>{{$organization['kind']}}, {{$organization['city']}},
                                        {{$organization['area']}}</a>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-8 py-xl-5">

        </div>
        <div class="col-md-8 py-xl-4">

        </div>
    </div>
@endsection
