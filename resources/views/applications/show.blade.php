@extends('layouts.app')

@section('content')
    <div class="container mx-auto">
        <div class="row">
            <div class="col-md-6">
                <h1 class="profile-header" style="font-weight: bolder">{{ $organization->name }}</h1>
                <i>{{ $organization->kind }}</i>
                <i>{{ $organization->city  }}</i>
                <i>{{ $organization->area  }}</i>
                <h3 class="py-3">Apie</h3>
                <p>{{ $organization->about }}</p>
                <h3 class="py-3">Geroji patirtis - Noriai dalinamės</h3>
                <p>{{ $organization->good_examples }}</p>
            </div>
        </div>

        @if(Auth::guard('organization')->user() || Auth::check())
            <div class="row>">
                <div class="col-md-12">
                    <div class="card border-0 mt-2">
                        <div class="card-header border-0">
                            <h2 class="py-2 my-0 text-center">
                                {{ __('Vizituoti') }}
                            </h2>
                        </div>

                        @if ($message = Session::get('success'))
                            <div class="alert alert-success text-center">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        <div class="card-body border-0">
                            <!--
                            <form method="POST" action=" route('organization.contact', $organization->id) ">
                            -->
                            <form method="POST"
                                  action="{{ url('/apply/'.$organization->id."/".$model."/".$model_id) }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-2 col-form-label text-md-right">{{ __('Prisistatykite įstaigai') }}</label>

                                    <div class="col-md-8">
                                    <textarea id="name" type="text"
                                              class="form-control @error('name') is-invalid @enderror" name="name"
                                              value="{{ old('name') }}" required autocomplete="name"
                                              autofocus
                                              placeholder="Šioje skiltyje trumpai prisistatykite ir nurodykite, kuo jus sudomina ši švietimo įstaiga."></textarea>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="description"
                                           class="col-md-2 col-form-label text-md-right">{{ __('Vizito lūkesčiai') }}</label>

                                    <div class="col-md-8">
                                    <textarea id="description" type="text"
                                              class="form-control @error('description') is-invalid @enderror"
                                              name="description" value="{{ old('description') }}" required
                                              autocomplete="description" autofocus
                                              placeholder="Šioje skiltyje aprašykite savo vizito siekius, tikslus, lūkesčius."></textarea>

                                        @error('description')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-2">
                                        <button type="submit" class="btn btn-outline-primary btn-contact">
                                            {{ __('Siųsti') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row py-5">
                <div class="col-md-12">
                    <h2 class="text-center">Noriu vizituoti</h2>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-lg-4 text-center">
                    <h4>Kaip švietimo įstaiga</h4>
                    <p>Skirta švietimo įstaigoms, kurios nori tiek vizituoti kitas, tiek sulaukti
                        vizituojančių.</p>
                </div>
                <div class="col-lg-4 text-center">
                    <h4>Kaip atskiras vizituojantis</h4>
                    <p>Skirta pavieniams asmenims - tėvams, mokiniams, švietimo darbuotojams,
                        žurnalistams, organizacijų atstovams, visuomenės veikėjams, žmonių grupės.</p>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-lg-4 text-center">
                    <form action="{{ url('/for-organizations') }}">
                        <button class="btn btn-info">Plačiau</button>
                    </form>
                </div>
                <div class="col-lg-4 text-center">
                    <form action="{{ url('/for-visiters') }}">
                        <button class="btn btn-info">Plačiau</button>
                    </form>
                </div>
            </div>
        @endif
    </div>
@endsection
