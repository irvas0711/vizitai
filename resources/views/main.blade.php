@extends('layouts.app')

@section('content')
    <!-- Main jumbotron for a primary marketing message or call to action -->

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Vizitavimo sistema</h1>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <hr class="my-4">
                        <p class="lead">Padedame atrasti dar neatrastas Lietuvos mokyklas ir švietimo įstaigas.
                            Nuo valstybinių iki alternatyvaus švietimo.</p>
                        <hr class="my-4">
                    </div>
                    <div class="col"></div>
                </div>

                <p style="text-align: center; font-weight: bold; font-size: larger">Išsirink įstaigą vizitui!</p>

                <form class="form-inline justify-content-md-center" action="{{ url('/show-organizations') }}">
                @csrf
                <!-- <div class="form-group d-inline-flex"> -->
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <label for="area"></label>
                            <select name="area" id="area" class="selectpicker mr-sm-1 @error('area') is-invalid @enderror" data-live-search="true"
                                    data-style="btn-primary">
                                <option value="" class="font-weight-bold">- Visos Savivaldybės -</option>
                                @foreach($areas as $area)
                                    @if($area != null)
                                    <option>{{ $area }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="col-auto">
                            <label for="kind"></label>
                            <select name="kind" id="kind" class="selectpicker mr-sm-1 @error('kind') is-invalid @enderror" data-live-search="true"
                                    data-style="btn-primary">
                                <option value="" class="font-weight-bold">- Visi įstaigų tipai -</option>
                                @foreach($kinds as $kind)
                                    @if($kind != null)
                                    <option>{{ $kind }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-secondary mr-sm-1" type="submit"
                                    style="background-color: darkblue">Ieškoti
                            </button>
                        </div>
                    </div>
                    <input type="hidden" name="search" value="1">
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row text-center">
            <h2 class="col-md-12">Kaip veikia vizitavimo sistema?</h2>
            <p>
                Vizitavimo sistema suteikia galimybę inicijuoti dialogą
                su pasirinkta švietimo įstaiga. Išsirinkite iš sąrašo jus
                dominančią švietimo organizaciją bei pateikite vizitavimo užklausą.
                Švietimo įstaiga gavusi jūsų užklausą turės galimybę su jumis
                susisiekti bei pratęsti bendradarbiavimą.
            </p>
        </div>

        <div class="row py-md-5">
            <div class="col-md-6">
                <h2>Švietimo įstaigoms</h2>
                <p>
                    Vizitavimo sistemoje kviečiame registruotis
                    įvairaus tipo švietimo įstaigas - valstybines ir
                    privačias, mokyklas, kurios taiko unikalias ugdymo
                    metodikas, jaunimo centrus.
                </p>
                <form action="{{ url('/for-organizations') }}">
                    <button class="btn btn-outline-info" >Plačiau</button>
                </form>
            </div>
            <div class="col-md-6">
                <h2>Vizituojantiems</h2>
                <p>
                    Vizituoti pasirinktas švietimo įstaigas yra kviečiami
                    plataus spektro žmonės - mokinių tėvai, studentai,
                    švietimo darbuotojai, žurnalistai, visuomenės veikėjai.
                    Visi, kas nori atrasti švietimą naujai.
                </p>
                <form action="{{ url('/for-visiters') }}">
                    <button class="btn btn-outline-info" >Plačiau</button>
                </form>
            </div>
        </div>
    </div> <!-- /container -->

@endsection

