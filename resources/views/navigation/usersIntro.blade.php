@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="display-6 my-3" style="font-weight: bolder">Noriu vizituoti</h1>
        <hr class="my-3">
        <div class="container">
            <div class="col-10">
                <h3 class="display-6 my-3" style="font-weight: bolder">Kaip tai vyksta?</h3>
                <li class="list-unstyled " style="font-size: medium">Sekite keliais papratais žingsniais:
                    <ul>
                        <li>Registruokitės.</li>
                        <li>Išsirinkite jums patikusią švietimo įstaigą</li>
                        <li>Įvardinkite, kuo patraukė jūsų dėmėsį pasirinkta įstaigą bei įvardinkite vizito lūkesčius.
                        </li>
                        <li>Išsiųskite vizitavimo užklausą ir laukite švietimo įstaigos atsakymo.</li>
                        <li>Gavote neigiamą atsakymą? Nenusiminkite ir ieškokite kitos įstaigos.</li>
                    </ul>
                </li>
            </div>
        </div>

        <!-- REGISTRATION CHOICES -->
        @if(!Auth::guard('organization')->user() && !Auth::guard('web')->user() == true)
        <form class="text-center">
            <div class="container py-2">
                <h4 class="display-6 my-3" style="font-weight: bolder">Tapk Vizitavimo sistemos dalimi</h4>
                <div class="col">
                    <a class="btn btn-outline-primary btn-lg" href="{{ route('login') }}">Prisijungti</a>
                </div>
                <div class="col py-3">
                    <a class="btn btn-primary btn-sm" href="{{ route('register') }}">Registruotis</a>
                </div>
            </div>
        </form>
        @endif
    </div>
@endsection
