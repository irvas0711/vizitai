@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="display-6 my-3" style="font-weight: bolder">Vizitavimo sistemos statistika</h1>
        <hr class="my-3">
        <div class="container">
            <div class="col-10">
                <h3 class="display-6 my-3" style="font-weight: bolder">Populiariausios švietimo įstaigos</h3>
                <h5 class="display-6 my-3" style="font-weight: bolder">Pagal gautų užklausų skaičių</h5>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Nr</th>
                        <th scope="col">Įstaiga</th>
                        <th scope="col">Gautų užklausų skaičius</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($organizations as $key => $value)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $key }}</td>
                            <td>{{ $value }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <h5 class="display-6 my-3" style="font-weight: bolder">Pagal geografiją</h5>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Nr</th>
                        <th scope="col">Savivaldybė</th>
                        <th scope="col">Gautų užklausų skaičius</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($areaList as $key => $value)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $key }}</td>
                            <td>{{ $value }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <h5 class="display-6 my-3" style="font-weight: bolder">Pagal įstaigos tipą</h5>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Nr</th>
                        <th scope="col">Tipas</th>
                        <th scope="col">Gautų užklausų skaičius</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($kindList as $key => $value)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $key }}</td>
                            <td>{{ $value }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <h5 class="display-6 my-3" style="font-weight: bolder">Didžiausio susidomėjimo klasės</h5>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Nr</th>
                        <th scope="col">Tipas</th>
                        <th scope="col">Gautų užklausų skaičius</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($classesList as $key => $value)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $key }}</td>
                            <td>{{ $value }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
