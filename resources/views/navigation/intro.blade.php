@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="display-6 my-3" style="font-weight: bolder">Norime sulaukti vizituojančių</h1>
        <hr class="my-3">
        <div class="container">
            <div class="col-10">
                <h3 class="display-6 my-3" style="font-weight: bolder">Kaip tai vyksta?</h3>
                <li class="list-unstyled " style="font-size: medium">Sekite keliais papratais žingsniais:
                    <ul>
                        <li>Užsiregistruokite ir per 48 valandas sulaukite registracijos patvirtinimo.</li>
                        <li>Kūrybiškai ir nuoširdžiai užpildykite savo švietimo įstaigos profilį.</li>
                        <li>Laukite viztavimo užklausų savo pašto dėžutėje arba ryžkitės vizituoti patys išsirinkę
                            patikusią įstaigą.
                        </li>
                        <li>Gavę užklausą tęskite tarimąsi dėl vizito taip, kaip jums patogiau - telefonu, el. paštu,
                            socialiniuose tinkluose.
                        </li>
                        <li>Gavus vizitavimo užklausą nesinori organizuoti šio vizito? Praneškite vizituojantiems dėl
                            šio sprendimo. Galbūt ši pažintis taps turininga ateityje?
                        </li>
                    </ul>
                </li>
            </div>
            <div class="col-10">
                <h3 class="display-6 my-3" style="font-weight: bolder">Gerosios patirtys! Kas tai?</h3>
                <p style="font-size: medium">Gerosios patirtys yra labai plati ir laisvai interpretuojama sąvoka.
                    Pažvelkime kūrybišai. Tai gali būti jūsų mokyklos unikali ugdymo metodika.</p>
            </div>
        </div>

        @if(!Auth::guard('organization')->user() && !Auth::guard('web')->user() == true)
        <div class="container py-2" style="margin-bottom: 76px">
            <form class="text-center">
                <h3 class="display-6 my-3" style="font-weight: bolder">Kviečiame registruotis</h3>
                <div class="row py-4">
                    <div class="col">
                        <p class="font-weight-normal" style="font-size: large"></p>
                    </div>
                    <div class="col">
                        <p class="font-weight-normal" style="font-size: large">Su Lietuvos mokykloms skirtais <b>lm.lt
                                adresų zonos</b> el. paštais</p>
                    </div>
                    <div class="col">
                        <p class="font-weight-normal" style="font-size: large"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <a></a>
                    </div>
                    <div class="col">
                        <a href="{{ route('organization.register') }}"
                           class="btn btn-primary my-2 my-sm-0">Registruotis</a>
                    </div>
                    <div class="col">
                        <a></a>
                    </div>
                </div>
            </form>
            <div class="row" style="margin-top: 50px">
                <div class="col">
                    <a style="font-weight: bold">Jau esi sistemoje?</a>
                </div>
            </div>
            <div class="row py-2">
                <div class="col">
                    <a href="{{ route('organization.login') }}" class="btn btn-outline-primary">Prisijungti</a>
                </div>
            </div>
        </div>
        @endif
    </div>
    <!-- REGISTRATION CHOICES -->
    <!-- Visi registracijos variantai
        <div class="container py-2" style="margin-bottom: 76px">
            <form class="text-center">
                <h3 class="display-6 my-3" style="font-weight: bolder">Kviečiame registruotis</h3>
                <div class="row py-4">
                    <div class="col">
                        <p class="font-weight-normal" style="font-size: large">Su Lietuvos mokyklų <b>@lm.lt</b> arba <b>@</b><i>[mokykla]</i><b>.vilnius.lm.lt</b> el. pašto adresais</p>
                    </div>
                    <div class="col">
                        <p class="font-weight-normal" style="font-size: large">Pateikiant <b>mokytojo pažymėjimo nuotrauką</b> bei <b>nuorodą į mokyklos puslapį</b>, kuriame yra nurodytas mokytojo vardas ir pavardė</p>
                    </div>
                    <div class="col">
                        <p class="font-weight-normal" style="font-size: large">Pateikiant <b>oficialų el. pašto adresą</b> bei <b>nuorodą į įstaigos puslapį</b>, kuriame yra registracijos el. paštas</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <a href="{{ route('organization.register') }}" class="btn btn-primary my-2 my-sm-0">Registruotis</a>
                    </div>
                    <div class="col">
                        <a class="btn btn-primary my-2 my-sm-0">Registruotis</a>
                    </div>
                    <div class="col">
                        <a class="btn btn-primary my-2 my-sm-0" >Registruotis</a>
                    </div>
                </div>
            </form>
            <div class="row" style="margin-top: 50px">
                <div class="col">
                    <a style="font-weight: bold">Jau esi sistemoje?</a>
                </div>
            </div>
            <div class="row py-2">
                <div class="col">
                    <a href="{{ route('organization.login') }}" class="btn btn-outline-primary">Prisijungti</a>
                </div>
            </div>
        </div>
        -->
@endsection
