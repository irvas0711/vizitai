<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Organization
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization newModelQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization newQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Organization query()
 */
	class Organization extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OrganizationVote
 *
 * @method navigation \Illuminate\Database\Eloquent\Builder|OrganizationVote newModelQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|OrganizationVote newQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|OrganizationVote query()
 */
	class OrganizationVote extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method navigation \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|User query()
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Visit
 *
 * @method navigation \Illuminate\Database\Eloquent\Builder|Visit newModelQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Visit newQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|Visit query()
 */
	class Visit extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\VisitApplication
 *
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication newModelQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication newQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitApplication query()
 */
	class VisitApplication extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\VisitRating
 *
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitRating newModelQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitRating newQuery()
 * @method navigation \Illuminate\Database\Eloquent\Builder|VisitRating query()
 */
	class VisitRating extends \Eloquent {}
}

