<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class VisitApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Vienas vizitavimo užklausos įrašas Demokratinei mokyklai
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '12',
            'applicable' => 'user',
            'organization_id' => '3',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '5',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '6',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '5',
            'applicable' => 'user',
            'organization_id' => '3',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '7',
            'applicable' => 'user',
            'organization_id' => '3',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);

        // Saulės gimnazija (3 vienetai)
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '2',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '2',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '2',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);

        // Pajūrio Privati (3 vienetai)
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '16',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '16',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '16',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        // Kopų mokykla (2 vienetai)
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '17',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '17',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);

        // Visit applications from users
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '15',
            'applicable' => 'user',
            'organization_id' => '5',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '15',
            'applicable' => 'user',
            'organization_id' => '4',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '15',
            'applicable' => 'user',
            'organization_id' => '2',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '15',
            'applicable' => 'user',
            'organization_id' => '1',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
            Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '4',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
            pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
             Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '12',
            'applicable' => 'user',
            'organization_id' => '4',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
             pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '13',
            'applicable' => 'user',
            'organization_id' => '4',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '5',
            'applicable' => 'user',
            'organization_id' => '4',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '1',
            'applicable' => 'user',
            'organization_id' => '4',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '2',
            'applicable' => 'user',
            'organization_id' => '5',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);

        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '4',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '12',
            'applicable' => 'user',
            'organization_id' => '19',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '13',
            'applicable' => 'user',
            'organization_id' => '19',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '5',
            'applicable' => 'user',
            'organization_id' => '11',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '1',
            'applicable' => 'user',
            'organization_id' => '11',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '2',
            'applicable' => 'user',
            'organization_id' => '12',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);

        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '8',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '12',
            'applicable' => 'user',
            'organization_id' => '8',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '13',
            'applicable' => 'user',
            'organization_id' => '13',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '14',
            'applicable' => 'user',
            'organization_id' => '16',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '1',
            'applicable' => 'user',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '2',
            'applicable' => 'user',
            'organization_id' => '15',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '3',
            'applicable' => 'user',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '4',
            'applicable' => 'user',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '5',
            'applicable' => 'user',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '6',
            'applicable' => 'user',
            'organization_id' => '6',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '7',
            'applicable' => 'user',
            'organization_id' => '4',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '8',
            'applicable' => 'user',
            'organization_id' => '7',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);

        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'user',
            'organization_id' => '2',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '12',
            'applicable' => 'user',
            'organization_id' => '20',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '7',
            'applicable' => 'user',
            'organization_id' => '8',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '14',
            'applicable' => 'user',
            'organization_id' => '7',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '1',
            'applicable' => 'user',
            'organization_id' => '6',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '2',
            'applicable' => 'user',
            'organization_id' => '15',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '3',
            'applicable' => 'user',
            'organization_id' => '18',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '4',
            'applicable' => 'user',
            'organization_id' => '18',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '5',
            'applicable' => 'user',
            'organization_id' => '18',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '6',
            'applicable' => 'user',
            'organization_id' => '17',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '7',
            'applicable' => 'user',
            'organization_id' => '13',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);

        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
             Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '8',
            'applicable' => 'user',
            'organization_id' => '13',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
             pristatytume savo diegiamus projektus.',
        ]);

        // Organizacijų vizitavimo užklausos įrašai

        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus.
             Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'organization',
            'organization_id' => '6',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį,
             pristatytume savo diegiamus projektus.',
        ]);

        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '5',
            'applicable' => 'organization',
            'organization_id' => '8',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '13',
            'applicable' => 'organization',
            'organization_id' => '13',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '14',
            'applicable' => 'organization',
            'organization_id' => '16',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '1',
            'applicable' => 'organization',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '2',
            'applicable' => 'organization',
            'organization_id' => '15',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '4',
            'applicable' => 'organization',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '5',
            'applicable' => 'organization',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '6',
            'applicable' => 'organization',
            'organization_id' => '6',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '7',
            'applicable' => 'organization',
            'organization_id' => '3',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '4',
            'applicable' => 'organization',
            'organization_id' => '3',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '14',
            'applicable' => 'organization',
            'organization_id' => '16',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);

        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '12',
            'applicable' => 'organization',
            'organization_id' => '9',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '13',
            'applicable' => 'organization',
            'organization_id' => '13',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '14',
            'applicable' => 'organization',
            'organization_id' => '16',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '1',
            'applicable' => 'organization',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '2',
            'applicable' => 'organization',
            'organization_id' => '15',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '3',
            'applicable' => 'organization',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '4',
            'applicable' => 'organization',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '5',
            'applicable' => 'organization',
            'organization_id' => '14',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '6',
            'applicable' => 'organization',
            'organization_id' => '6',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '7',
            'applicable' => 'organization',
            'organization_id' => '4',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '12',
            'applicable' => 'organization',
            'organization_id' => '5',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '11',
            'applicable' => 'organization',
            'organization_id' => '7',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '3',
            'applicable' => 'organization',
            'organization_id' => '9',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '3',
            'applicable' => 'organization',
            'organization_id' => '10',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
        DB::table("visit_applications")->insert([
            'about' => 'Esame švietimo veikėjai, vykdome švietimo naujovių diegimo projektus. Mus sudomino jūsų mokyklos ugdymo filosofija.',
            'applicable_id' => '8',
            'applicable' => 'organization',
            'organization_id' => '12',
            'description' => 'Vizito metu norėtume geriau suvokti jūsų mokyklos modelį, pristatytume savo diegiamus projektus.',
        ]);
    }
}
