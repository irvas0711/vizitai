<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("areas")->insert([
            'name' => 'Akmenės rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Alytaus miesto savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Alytaus rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Anykščių rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Birštono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Biržų rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Druskininkų savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Elektrėnų savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Ignalinos rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Jonavos rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Joniškio rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Jurbarko rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Kaišiadorių rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Kalvarijos savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Kauno miesto savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Kauno rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Kazlų Rūdos savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Kėdainių rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Kelmės rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Klaipėdos miesto savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Klaipėdos rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Kretingos rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Kupiškio rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Lazdijų rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Marijampolės savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Mažeikių rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Molėtų rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Neringos savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Pagėgių savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Pakruojo rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Palangos miesto savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Panevėžio miesto savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Panevėžio rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Pasvalio rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Plungės rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Prienų rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Radviliškio rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Raseinių rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Rietavo savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Rokiškio rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Skuodo rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Šakių rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Šalčininkų rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Šiaulių miesto savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Šiaulių rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Šilalės rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Šilutės rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Širvintų rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Švenčionių rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Tauragės rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Telšių rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Trakų rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Ukmergės rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Utenos rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Varėnos rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Vilkaviškio rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Vilniaus miesto savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Vilniaus rajono savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Visagino savivaldybė']);
        DB::table("areas")->insert([
            'name' => 'Zarasų rajono savivaldybė']);
    }
}
