<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
            'name' => 'Irmantas',
            'surname' => 'Vasiljevas',
            'email' => 'irvas0711@gmail.com',
            'password' => \Hash::make("Pas1234*"),
            'type' => 'administrator',
            'kind'=> 'Studentas',
            'organization' => 'VU Televizija'
        ]);
        DB::table("users")->insert([
            'name' => 'Gediminas',
            'surname' => 'Skačkauskas',
            'email' => 'gedas@gmail.com',
            'password' => \Hash::make("Pas1234*"),
            'kind'=> 'Žurnalistas',
            'organization' => 'LRT Televizija'
        ]);
        DB::table("users")->insert([
            'name' => 'Rolandas',
            'surname' => 'Žilinskas',
            'email' => 'rolandas@gmail.com',
            'password' => \Hash::make("Pas1234*"),
            'kind' => 'Tėvai',
            'organization' => 'Visagino „Verdenės“ gimnazija'
        ]);
        DB::table("users")->insert([
            'name' => 'Karolina',
            'surname' => 'Keršytė',
            'email' => 'karolina@gmail.com',
            'password' => \Hash::make("Pas1234*"),
            'kind' => 'Studentas',
            'organization' => 'VU Studentų atstovybė'
        ]);
        DB::table("users")->insert([
            'name' => 'Jonas',
            'surname' => 'Klimavičius',
            'email' => 'jonas@gmail.com',
            'password' => \Hash::make("Pas1234*"),
            'kind' => 'Studentas',
            'organization' => 'Švietimas vaikams'
        ]);
    }
}
