<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use phpDocumentor\Reflection\Types\Nullable;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Vilniaus Miesto
        DB::table("organizations")->insert([
            'name' => 'Vilniaus Licėjus',
            'email' => 'info@licejus.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Vilnius',
            'area' => 'Vilniaus miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Valstybinė',
            'classes' => '4',
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Vilniaus Saulės Gimnazija',
            'email' => 'info@vsg.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Vilnius',
            'area' => 'Vilniaus miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Privati',
            'classes' => '4',
            'midst' => 1,
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Demokratinė mokykla',
            'email' => 'info@dm.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Aukštieji Paneriai',
            'area' => 'Vilniaus miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Alternatyvaus švietimo',
            'classes' => '4',
            'preschool' => 1,
            'primary' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Žalianamis',
            'email' => 'info@zalianamis.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Vilnius',
            'area' => 'Vilniaus miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Jaunimo centras',
            'classes' => null,
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Vilniaus Valdorfo Mokykla',
            'email' => 'info@vvm.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Vilnius',
            'area' => 'Vilniaus miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Alternatyvaus švietimo',
            'classes' => '4',
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Vilniaus Vytauto Didžiojo Gimnazija',
            'email' => 'info@vdg.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Vilnius',
            'area' => 'Vilniaus miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Privati',
            'classes' => '4',
            'primary' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Vilniaus Neries Progimnazija',
            'email' => 'info@vdg.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Vilnius',
            'area' => 'Vilniaus miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Valstybinė',
            'classes' => '1,2',
            'primary' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);

        // Kauno miesto savivaldybė
        DB::table("organizations")->insert([
            'name' => 'Žalioji gamtos mokykla',
            'email' => 'info@zgm.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Smiltynai',
            'area' => 'Kauno rajono savivaldybė',
            'registration_picture' => null,
            'web_link' => 'https://getbootstrap.com/docs/4.5',
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Alternatyvaus švietimo',
            'classes' => '0,1',
            'preschool' => 1,
            'primary' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'KTU Gimnazija',
            'email' => 'info@ktug.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Kaunas',
            'area' => 'Kauno miesto savivaldybė',
            'registration_picture' => null,
            'web_link' => 'https://getbootstrap.com/docs/4.5',
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Valstybinė',
            'classes' => '0,1',
            'primary' => 1,
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Kauno Saulės Gimnazija',
            'email' => 'info@ksg.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Kaunas',
            'area' => 'Kauno miesto savivaldybė',
            'registration_picture' => null,
            'web_link' => 'https://getbootstrap.com/docs/4.5',
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Privati',
            'classes' => '0,1',
            'primary' => 1,
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Atviras jaunimo centras „Vartai“',
            'email' => 'info@vartai.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Kaunas',
            'area' => 'Kauno miesto savivaldybė',
            'registration_picture' => null,
            'web_link' => 'https://getbootstrap.com/docs/4.5',
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Jaunimo centras',
            'classes' => '0,1',
            'primary' => 1,
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Kauno Valdorfo mokykla',
            'email' => 'info@kvm.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Kaunas',
            'area' => 'Kauno miesto savivaldybė',
            'registration_picture' => null,
            'web_link' => 'https://getbootstrap.com/docs/4.5',
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Alternatyvaus švietimo',
            'classes' => '0,1',
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Kauno Tado Ivanausko progimnazija',
            'email' => 'info@ktip.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Kaunas',
            'area' => 'Kauno miesto savivaldybė',
            'registration_picture' => null,
            'web_link' => 'https://getbootstrap.com/docs/4.5',
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Valstybinė',
            'classes' => '0,1',
            'midst' => 1,
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        // Klaipėdos miesto savivaldybė
        DB::table("organizations")->insert([
            'name' => 'Klaipėdos Licėjus',
            'email' => 'info@klaipedoslicejsu.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Klaipėda',
            'area' => 'Klaipėdos miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Valstybinė',
            'classes' => '4',
            'midst' => 1,
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Klaipėdos Ąžuolyno Gimnazija',
            'email' => 'info@azuolynogimnazija.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Klaipėda',
            'area' => 'Klaipėdos miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Valstybinė',
            'classes' => '4',
            'primary' => 1,
            'midst' => 1,
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Pajūrio Privati Gimnazija',
            'email' => 'info@azuolynogimnazija.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Klaipėda',
            'area' => 'Klaipėdos miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Privati',
            'classes' => '4',
            'preschool' => 1,
            'primary' => 1,
            'midst' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Kopų mokykla',
            'email' => 'info@km.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Klaipėda',
            'area' => 'Klaipėdos miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Alternatyvaus švietimo',
            'classes' => '4',
            'preschool' => 1,
            'primary' => 1,
            'midst' => 1,
            'gymnasium' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        // Kiti miestai
        DB::table("organizations")->insert([
            'name' => 'Panevėžio Juozo Miltinio Gimnazija',
            'email' => 'info@km.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Panevežys',
            'area' => 'Panevėžio miesto savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Valstybinė',
            'classes' => '4',
            'preschool' => 1,
            'primary' => 1,
            'midst' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Zarasų Ąžuolo Gimnazija',
            'email' => 'info@zag.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Zarasai',
            'area' => 'Zarasų rajono savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Valstybinė',
            'classes' => '4',
            'primary' => 1,
            'midst' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Druskininkų Ryto gimnazija',
            'email' => 'info@drg.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Druskininkai',
            'area' => 'Druskininkų savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Valstybinė',
            'classes' => '4',
            'preschool' => 1,
            'primary' => 1,
            'midst' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
        DB::table("organizations")->insert([
            'name' => 'Vilkaviškio Aušros Gimnazija',
            'email' => 'info@vag.lt',
            'password' => \Hash::make("123456789"),
            'city' => 'Vilkaviškis',
            'area' => 'Vilkaviškio rajono savivaldybė',
            'registration_picture' => null,
            'about' => 'Mūsų misija - Ugdyti autentišką žmogų, mylintį save, kitus ir supantį pasaulį bei siekiantį nuolat tobulėti. Akademinių rezultatų siekiame kūrybiškai, pasitelkdami inovatyvius ugdymo metodus ir patyriminę praktiką.',
            'good_examples' => 'Vykdome projektą projektą „Demokratinio ugdymo principais grįstų praktikų įdiegimas Lietuvoje“ arba trumpiau – „DU Lietuvoje“, kurio tikslas – pilietiškai aktyvi visuomenė. Tikime, kad ją galime puoselėti diegiant mokyklose demokratinio ugdymo principais grįstas praktikas, kurios mokinius mokytų pilietinio ugdymo per praktinę kasdienę veiklą mokykloje, kad mokiniai pajustų, kas yra realus kad ir mažų sprendimų priėmimas, darantis įtaką bendruomenės gyvenimui. ',
            'kind' => 'Valstybinė',
            'classes' => '4',
            'primary' => 1,
            'midst' => 1,
            'application_sum' => '0',
            // 'vote_sum' => '0',
            // 'vote_quantity' => '0'
        ]);
    }
}
