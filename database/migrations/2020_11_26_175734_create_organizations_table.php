<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->id()->unique();
            $table->string('name');
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('city')->nullable();
            $table->string('area')->nullable();
            $table->string('registration_picture')->nullable();
            $table->string('web_link')->nullable();
            $table->text('about')->nullable();
            $table->text('good_examples')->nullable();
            $table->string('kind')->nullable();
            $table->string('classes')->nullable();
            $table->boolean('preschool')->default(0);
            $table->boolean('primary')->default(0);
            $table->boolean('midst')->default(0);
            $table->boolean('gymnasium')->default(0);
            $table->integer('application_sum')->nullable();

       //    $table->integer('vote_sum')->nullable();
       //     $table->integer('vote_quantity')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
