<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_applications', function (Blueprint $table) {
            $table->id();
            $table->longText('about');
            $table->bigInteger('applicable_id');
            $table->string('applicable');
            $table->bigInteger('organization_id');
            $table->longText('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     *         'name',
    'user_id',
    'organization_id',
    'message',
    'visit_status'
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visit_applications');
    }
}
